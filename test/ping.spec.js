const {expect} = require('chai');
const {requester} = require('./bootstrap');


describe('ping', () => {
  it('should be able to respond to ping request', async () => {
    const res = await requester
      .get('/ping');
    expect(res).to.have.status(200);
  });
});
