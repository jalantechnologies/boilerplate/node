process.env.NODE_CONFIG_ENV = 'testing';

const chai = require('chai');
const mongoose = require('mongoose');
const chaiHttp = require('chai-http');

const server = require('../src/app');

chai.use(chaiHttp);

const requester = chai.request(server).keepOpen();

after(async () => {
  await mongoose.disconnect();
  await requester.close();
});

module.exports = {requester};
