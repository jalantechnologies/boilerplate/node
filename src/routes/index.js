const accounts = require('./accounts');
const account = require('./account');
const profile = require('./profile');
const ping = require('./ping');

module.exports = {
  accounts,
  account,
  profile,
  ping,
};
