const express = require('express');

const router = express.Router({});

router.get('/', (req, res) => {
  res.send({
    success: res.__('MESSAGES.PING_REPORT'),
  });
});

module.exports = router;
