#!/bin/bash
curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
cat <<EOT>> ~/.netrc
machine api.heroku.com
  login $HEROKU_LOGIN_ID
  password $HEROKU_API_KEY
EOT

heroku apps:create api-beta-$APP_NAME-$CI_MERGE_REQUEST_IID

heroku pipelines:add $HEROKU_PIPELINE -a api-beta-$APP_NAME-$CI_MERGE_REQUEST_IID -s staging

heroku config -s -a $HEROKU_APP_STAGING > config.txt
cat config.txt | tr '\n' ' ' | xargs heroku config:set -a api-beta-$APP_NAME-$CI_MERGE_REQUEST_IID
