#!/bin/bash
curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
cat <<EOT>> ~/.netrc
machine api.heroku.com
  login $HEROKU_LOGIN_ID
  password $HEROKU_API_KEY
EOT

heroku apps:destroy -a api-beta-$APP_NAME-$CI_MERGE_REQUEST_IID -c api-beta-$APP_NAME-$CI_MERGE_REQUEST_IID